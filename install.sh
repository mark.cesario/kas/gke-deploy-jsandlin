#!/bin/bash


#####
# This file is a wrapper for loading required configs then calling Terraform.
# The purpose of this file is to locally replicate the way the .gitlab-ci.yml
#     will execute your Terraform.
# See README.md for requirements & how to use.
#####
# while getopts 'd' flag; do
#   case "${flag}" in
#     d) set -x; TF_LOG=TRACE; # rm -fr .terraform*
#   esac
# done
# If DEBUG is set, enable logging

set -e
export TF_LOG=ERROR
if [ "$DEBUG" == "TRUE" ] || [ "$DEBUG" == "1" ]; then
  set -x 
  export TF_LOG=TRACE
  rm -fr .terraform/modules/*
  rm -f .terraform.lock.hcl
fi

#######
# Credential files
#######
export GOOGLE_APPLICATION_CREDENTIALS=$HOME/.gcp/group-cs-9b54eb-ee0e183079b0.json
export GITLAB_CREDENTIALS="$HOME/.gitlab/.creds"


#######
# Load Google Cloud Credentials.
#######
function load_google_creds () {
  if [[ ! -f "$GOOGLE_APPLICATION_CREDENTIALS" ]]
  then
    echo "Cannot find Google App Creds file: "  $GOOGLE_APPLICATION_CREDENTIALS
    exit 1
  else
    export GOOGLE_APPLICATION_CREDENTIALS
    export TF_VAR_service_account_file=$GOOGLE_APPLICATION_CREDENTIALS
  fi

}
#######
# Load GitLab Credentials.
#######
function load_gitlab_creds () {
  if [[ ! -f "$GITLAB_CREDENTIALS" ]]
  then
    echo "Cannot find GitLab Creds file: "  $GITLAB_CREDENTIALS
    exit 1
  else
    . $GITLAB_CREDENTIALS
    export TF_VAR_gitlab_token=$password
  fi
}

function tf_init_plan () {
    
  pf=myplan.tfplan
  rm -f $pf
  terraform init -backend-config=$GITLAB_CREDENTIALS
  terraform plan --out $pf -lock=false

}

load_google_creds
load_gitlab_creds
tf_init_plan

if [[ "$1" =~ ^(create|apply$) ]]
then
  terraform apply --auto-approve
elif [[ "$1" == "refresh" ]]
then
  terraform refresh
elif [[ "$1" == "show" ]]
then
  terraform state show
elif [[ "$1" == "destroy" ]]
then
  terraform destroy --auto-approve -lock=false
fi

unset DEBUG
set -e
export TF_LOG=ERROR
