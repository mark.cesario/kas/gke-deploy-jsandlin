/*
* Create GKE Cluster & grant bindings required for internal management.
*/

# GKE cluster
resource "google_container_cluster" "primary" {
  name     = var.cluster_name
  location = var.region
  
  # We can't create a cluster with no node pool defined, but we want to only use
  # separately managed node pools. So we create the smallest possible default
  # node pool and immediately delete it.
  remove_default_node_pool = true
  initial_node_count       = 1

  network    = google_compute_network.vpc.name
  subnetwork = google_compute_subnetwork.subnet.name
  
  master_auth {
    client_certificate_config {
      issue_client_certificate = true
    }
  }
}

# 2021-12-22T13:17:11.264-0800 [WARN]  Provider "provider[\"registry.terraform.io/hashicorp/google\"]" produced an unexpected new value for google_container_node_pool.primary_nodes, but we are tolerating it because it is using the legacy plugin SDK.
#     The following problems may be the cause of any confusing errors from downstream operations:
#       - .node_config[0].min_cpu_platform: was null, but now cty.StringVal("")
#       - .node_config[0].node_group: was null, but now cty.StringVal("")
      # - .subject[0].namespace: planned value cty.StringVal("default") for a non-computed attribute
      # - .subject[1].namespace: planned value cty.StringVal("default") for a non-computed attribute
# 2021-12-22T13:17:11.264-0800 [TRACE] NodeAbstractResouceInstance.writeResourceInstanceState to workingState for google_container_node_pool.

# Separately Managed Node Pool
resource "google_container_node_pool" "primary_nodes" {
  name       = "${var.cluster_name}-node-pool"
  location   = var.region
  cluster    = google_container_cluster.primary.name
  node_count = var.gke_num_nodes

  node_config {
    # Google recommends custom service accounts that have cloud-platform scope and permissions granted via IAM Roles.
    service_account = google_service_account.sa.email

    oauth_scopes = [
      "https://www.googleapis.com/auth/cloud-platform",
      "https://www.googleapis.com/auth/compute",
      "https://www.googleapis.com/auth/devstorage.read_only",
      "https://www.googleapis.com/auth/logging.write",
      "https://www.googleapis.com/auth/monitoring",
    ]

    labels = {
      env = var.project
    }

    preemptible  = true
    machine_type = "n1-standard-1"
    tags         = ["${var.cluster_name}-node", "${var.project}-gke"]
    metadata = {
      disable-legacy-endpoints = "true"
    }
  }
}

# Terraform needs to manage K8S RBAC
# https://cloud.google.com/kubernetes-engine/docs/how-to/role-based-access-control#iam-rolebinding-bootstrap
resource "kubernetes_cluster_role_binding" "terraform_clusteradmin" {
  provider = kubernetes.thiscluster
  depends_on = [
    google_container_node_pool.primary_nodes
  ]

  metadata {
    name = "cluster-admin-binding-terraform"
  }


  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind      = "ClusterRole"
    name      = "cluster-admin"
  }

  # # email: required for gcloud + kubectl using the SA
  # subject {
  #   api_group = "rbac.authorization.k8s.io"
  #   kind      = "User"
  #   name      = google_service_account.sa.email
  # }

  # unique_id: required for Terraform using the SA
  subject {
    api_group = "rbac.authorization.k8s.io"
    kind      = "User"
    name      = google_service_account.sa.unique_id
  }
}



resource "kubernetes_cluster_role" "GitLabClusterMgtRole" {
  provider = kubernetes.thiscluster
  metadata {
    name = "GitLabClusterMgt"
    labels = {
      # Add these permissions to the "admin" and "edit" default roles.
      "rbac.authorization.k8s.io/aggregate-to-admin" = "true"
    }
  }

# endpoints
# Error: clusterroles.rbac.authorization.k8s.io "prometheus-prometheus-server" is forbidden: user "system:serviceaccount:gitlab-kubernetes-agent:gitlab-agent" (groups=["system:serviceaccounts" "system:serviceaccounts:gitlab-kubernetes-agent" "system:authenticated"]) is attempting to grant RBAC permissions not currently held:
#  {NonResourceURLs:["/metrics"], Verbs:["get"]}

  rule {
    api_groups = [""]
    resources  = ["endpoints"]
    verbs      = ["get", "list", "watch"]
  }
  rule {
    # This rule is required for fluentd installation.
    # podsecuritypolicies
    # Error: clusterroles.rbac.authorization.k8s.io "certmanager-cert-manager-controller-approve:cert-manager-io" is forbidden: user "system:serviceaccount:gitlab-kubernetes-agent:gitlab-agent" (groups=["system:serviceaccounts" "system:serviceaccounts:gitlab-kubernetes-agent" "system:authenticated"]) is attempting to grant RBAC permissions not currently held:
    #   {APIGroups:["cert-manager.io"], Resources:["signers"], ResourceNames:["clusterissuers.cert-manager.io/*"], Verbs:["approve"]}
    #   {APIGroups:["cert-manager.io"], Resources:["signers"], ResourceNames:["issuers.cert-manager.io/*"], Verbs:["approve"]}
    api_groups = ["cert-manager.io"]
    resources  = ["signers"]
    verbs      = ["approve", "sign"]
    
  }
  rule {
    # This rule is required for fluentd installation.
    # podsecuritypolicies
    # Error: roles.rbac.authorization.k8s.io "fluentd" is forbidden: user "system:serviceaccount:gitlab-kubernetes-agent:gitlab-agent" (groups=["system:serviceaccounts" "system:serviceaccounts:gitlab-kubernetes-agent" "system:authenticated"]) is attempting to grant RBAC permissions not currently held:
    #  {APIGroups:["extensions"], Resources:["podsecuritypolicies"], ResourceNames:["fluentd"], Verbs:["use"]}
    api_groups = [""]
    resources  = ["podsecuritypolicies"]
    verbs      = ["get", "list", "watch", "use"]
    
  }
}

resource "kubernetes_cluster_role_binding" "GitLabClusterMgtBinding" {
  provider = kubernetes.thiscluster
  metadata {
    name = "GitLabClusterMgt"
  }
  # role_ref => The ClusterRole to bind subjects to
  role_ref {
    api_group = "rbac.authorization.k8s.io"
    kind = "ClusterRole"
    name = kubernetes_cluster_role.GitLabClusterMgtRole.metadata[0].name
  }

  # subject => The service account to grant permissions to
  subject {
    api_group = "rbac.authorization.k8s.io"
    kind  = "User"
    name = google_service_account.sa.unique_id
  }
}



