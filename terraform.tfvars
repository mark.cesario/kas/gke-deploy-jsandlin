# Your Google Cloud settings
project = "group-cs-9b54eb"
region  = "us-west2"
zone    = "us-west2-a"

# The project holding the GitLab Agent configs. 
# For this project, the value is the ID of this project.
gitlab_project_id_agent_config = 33593486 

# Network information
subnet_cidr   = "10.56.0.0/14"

# GKE Cluster information
cluster_name = "mcesario-cluster-nonkas"
gke_num_nodes = 2

# Your GraphQL URL
gitlab_graphql_api_url = "https://gitlab.com/api/graphql"
