output "region" {
  value       = var.region
  description = "GCloud Region"
}

output "project" {
  value       = var.project
  description = "GCloud Project ID"
}

output "kubernetes_cluster_name" {
  value       = google_container_cluster.primary.name
  description = "GKE Cluster Name"
}

output "kubernetes_cluster_host" {
  value       = google_container_cluster.primary.endpoint
  description = "GKE Cluster Host"
}

output "kubernetes_server_defined_url" {
  value = google_container_cluster.primary.self_link
  description = "The server-defined URL for the resource."
}

output "gcloud_config_cmd" {
  value = "gcloud container clusters get-credentials ${google_container_cluster.primary.name}"
  description = "GCloud cmd to run to download your cluster creds"
}
# output "cluster_ca_certificate" {
#   value = local_file.ca_cert.filename
#   description = "Base64 encoded public certificate that is the root of trust for the cluster."
# }

# output "sa_private_key" {
#   value = local_file.sa_private_key.filename
#   description = "Base64 encoded private key used by clients to authenticate to the cluster endpoint."
# }

# output "sa_private_key_b64" {
#   value = local_file.sa_private_key_b64.filename
#   description = "Base64 encoded private key used by clients to authenticate to the cluster endpoint."
# }

# output "GitLabClusterMgtRole" {
#     value = kubernetes_cluster_role.GitLabClusterMgtRole
# }