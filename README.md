<<<<<<< HEAD
# GitLab Agent for Kubernetes - mnce edition
=======
# GitLab Agent for Kubernetes - mce edition
>>>>>>> be66e016b256ab398b0d93bba59a2b3647576296

## Overview
This instruction set is written to guide you through the entire process involved in setting up and integrating a Kubernetes Cluster with GitLab. The official documentation is available on GitLab under [GitLab Agent for Kubernetes](https://docs.gitlab.com/ee/user/clusters/agent/)

In the enterprise, creation of the infra & agent registration would be done seperately. However, by creating the cluster & setting up the Agent in the same TF run, I take advantage of [variables pulled from the creation](https://gitlab.com/sandlin/gke-deploy-env/-/blob/groot/main.tf#L57-59).

The `.gitlab-ci.yml` of this repo is configured to pull the Google Cloud Service Account JSON and the GitLab Service Account token from Hashicorp Vault.

## Pre-Requisites

### Google Cloud

##### Required Software
1. [Google Cloud SDK (gcloud CLI)](https://cloud.google.com/sdk/docs/install)
1. [Terraform CLI](https://learn.hashicorp.com/tutorials/terraform/install-cli)
1. [JQ](https://stedolan.github.io/jq/download/)

##### GCP Project
1. Take note of your Google Cloud Project ID. Moving forward, this will be referred to as `GCP_PROJECT_ID`.
 
##### Service Account
1. [Create a service account & download key JSON](https://cloud.google.com/docs/authentication/getting-started#creating_a_service_account). Moving forward, this will be referred to as `GCP_SERVICE_ACCOUNT`
1. Save the json file to `$HOME/.gcp` directory
1. Create a Base64 copy of your JSON key.
   
   `base64 $HOME/.gcp/<GCP_SERVICE_ACCOUNT>.json > $HOME/.gcp/<GCP_SERVICE_ACCOUNT>.json.b64`

   ex:

   `base64 $HOME/.gcp/jsandlin-admin.json > $HOME/.gcp/jsandlin-admin.json.b64`
1. Grant Service Account the required IAM Policies.
  - TODO: Currently this is granting more privs than required. Trim this down to bare minimum.
  1. Modify [iam_policy.json](./service_account/iam_policy.json) & ensure the `serviceAccount` is yours.
  1. Set the policies.

     `bash ./prereq/iam_policy_binding.sh -p <GCP_PROJECT_ID> -a <GCP_SERVICE_ACCOUNT>`
  
     ex:
  
     `bash ./prereq/iam_policy_binding.sh -p jsandlin-c9fe7132 -a jsandlin-admin`
  
     - TODO: Break this down to permissions in a custom role (to increase security)
     - [REF](https://cloud.google.com/iam/docs/understanding-roles#predefined_roles)
  1. Enable required services.

     `bash ./prereq/services_enable.sh -p <GCP_PROJECT_ID>`
  
     ex:
  
     `bash ./prereq/services_enable.sh -p jsandlin-c9fe7132`

### GitLab Access Token
1. Create a [Personal Access Token](https://docs.gitlab.com/ee/user/profile/personal_access_tokens.html) 
   - Under `Select scopes`, select `api` and `read_registry`  
   - Save this token in your password management tool. It will not be accessable after this page is closed.
1. Create a local file `~/.gitlab/.creds` using this `Access Token`.
    ```
    # GitLab API token
    username="jsandlin"
    password="glpat-QerShxqUsPcAvfAp3xN3P"
    ```

<!-- ### Terraform Module Token
1. Create a local file in `~/.terraformrc` using the `Access Token` above.
   ```
   credentials "gitlab.com" {
     token = "glpat-123456"
   }
   ``` -->
---

## GitLab Project Creation

### Overview

We will create 3 projects in GitLab:

1. Infrastructure Management
   - This project will create the required Google Cloud Infrastructure then install the GitLab Agent for Kubernetes & register this Agent with GitLab.
1. Cluster App Management
   - This project will manage the services in the GKE cluster, such as Ingress.
1. Web App
   - This project will deploy a Flask application to the cluster.

> note: The names above are not hard coded; simply example names.

> Ensure the two projects managing the cluster are created at the root level from which you want access.

## Infrastructure Management Project / Create Cluster & Install Agent
  1. Create a new GitLab Project by mirroring [this project](https://gitlab.com/sandlin/gke-deploy-env)
  1. Copy the `Project ID` from the index page of your project, directly under your project name.
  1. Modify [terraform.tfvars](./terraform.tfvars)
     1. Change [Google Cloud Settings](./terraform.tfvars#L1-4) Accordingly.
     2. Set the [gitlab_project_id_agent_config](./terraform.tfvars#L7) to the `Project ID` value copied above.  
  1. Modify [main.tf](./main.tf#L6-8) so Terraform will use GitLab for state management.
     1. Replace the project ID `32591477` with the `Project ID` value copied above.
  1. Modify the [gitlab-agent config](.gitlab/agents/gitlab-agent/config.yaml)
     1. Set the [gitops.manifest_projects.id](.gitlab/agents/gitlab-agent/config.yaml#L6) to the path of this project.
     1. Set the [ci_access.groups.id](.gitlab/agents/gitlab-agent/config.yaml#L14) to limit which group/subgroups can utilize this cluster.
  1. Modify the [install.sh](./install.sh) 
     1. Set the [GOOGLE_APPLICATION_CREDENTIALS](./install.sh#L21) to the path of your Base64 encoded credential json
  1. Test your changes by executing `./install.sh` with no args.
     1. This will validate GitLab and GCP integration.
  1. Commit and push your changes.
  1. Create your cluster
     ```
     ./install.sh create
     ```

  > *TODO*: Document how to configure Vault & the `.gitlab-ci.yml`. This is not being done now because I'm using Hashicorp Vault & need to document another process.

---

## Cluster App Management Project
  1. Create a new GitLab Project using the [Cluster Management Template](https://gitlab.com/gitlab-org/project-templates/cluster-management)
  1. Edit the `helmfile.yml` and uncomment `ingress`
      ```
      helmfiles:
        ...
        - path: applications/ingress/helmfile.yaml
        ...
      ```
  1. Edit the `.gitlab-ci.yml` 
     1. Uncomment the [variables.KUBE_CONTEXT](https://gitlab.com/gitlab-org/project-templates/cluster-management/-/blob/master/.gitlab-ci.yml#L8-9)
     1. Set the [KUBE_CONTEXT](https://gitlab.com/gitlab-org/project-templates/cluster-management/-/blob/master/.gitlab-ci.yml#L9) value to the context created above.
        [Example Project](https://gitlab.com/sandlin/gke-deploy-env-mgt/-/blob/master/.gitlab-ci.yml#L8-9)
  1. Save and commit / push the files to `master`.
     - This will trigger a Pipeline execution and install the INGRESS to your cluster.
  1. When this execution is completed, get the external IP of your INGRESS. Save this value for our project variables.
     ```
     # Make sure you are in the gitlab-managed-apps namespace
     kubens gitlab-managed-apps
     # Get the IP of the ingress.
     kubectl get services -o jsonpath='{..loadBalancer.ingress..ip}' ingress-nginx-ingress-controller
     35.247.53.168
     ```

### If you are using a Registered Domain Name
  1. Now you can setup your DNS Records with an A record pointing to this IP address.
     ex:
     ```
     $ gcloud dns record-sets list --zone=papanca
      NAME            TYPE  TTL    DATA
      ...
      *.papanca.com.  A     300    35.247.53.168
     ```

   If you have an A-Record already, the way to reset it is:
   ```
   gcloud dns record-sets delete "*.papanca.com." --type=A --zone=papanca; gcloud dns record-sets create "*.papanca.com." --type=A --zone=papanca --rrdatas=`kubectl get services -o jsonpath='{..loadBalancer.ingress..ip}' ingress-nginx-ingress-controller`
   ```
---
## Web App
  - An example app is located [here](https://gitlab.com/sandlin/examples/python_flask)
  - In the `.gitlab-ci.yml` file or in the project's CI/CD settings, define the following variables:
    ```
      KUBE_CONTEXT: "${CI_PROJECT_PATH}:${YOUR_AGENT_NAME}"
      KUBE_INGRESS_BASE_DOMAIN: '<Your Domain Name> OR <The Ingress IP Address>.nip.io'
      CI_KUBERNETES_ACTIVE: 'true'
      KUBE_NAMESPACE: "${CI_PROJECT_ID}-${CI_ENVIRONMENT_SLUG}"
    ```
    An example:
    ```
      KUBE_CONTEXT: "sandlin/gke-deploy-env:gitlab-agent"
      KUBE_INGRESS_BASE_DOMAIN: 'papanca.com'
      CI_KUBERNETES_ACTIVE: 'true'
      KUBE_NAMESPACE: "${CI_PROJECT_ID}-${CI_ENVIRONMENT_SLUG}"
    ```
  - If using [Auto DevOps](https://docs.gitlab.com/ee/topics/autodevops/#auto-devops-features), your app should now successfully deploy.

---

## Debugging

When executing the Terraform to create the cluster, if you see an error like:
  ```
    022-02-01T12:23:51.968-0500 [ERROR] vertex "kubernetes_cluster_role_binding.terraform_clusteradmin" error: Unauthorized
  ╷
  │ Error: Unauthorized
  │ 
  │   with kubernetes_cluster_role_binding.terraform_clusteradmin,
  │   on gke_cluster.tf line 68, in resource "kubernetes_cluster_role_binding" "terraform_clusteradmin":
  │   68: resource "kubernetes_cluster_role_binding" "terraform_clusteradmin" {
  │ 
  ╵
  ╷
  │ Error: Unauthorized
  │ 
  │   with kubernetes_cluster_role.GitLabClusterMgtRole,
  │   on gke_cluster.tf line 107, in resource "kubernetes_cluster_role" "GitLabClusterMgtRole":
  │  107: resource "kubernetes_cluster_role" "GitLabClusterMgtRole" {
  │
  ``` 

This is due to the cluster not being up and running 100%. Just rerun the `./install.sh create` and things should complete successfully.
- TODO: Find ref link.
- [One issue discussing EKS](https://github.com/hashicorp/terraform-provider-kubernetes/issues/918)


When trying to delete, the call to graphql can fail.
```
$ ./install.sh destroy                                          
...
  - region                        = "us-west1" -> null
module.gitlab_kubernetes_agent.graphql_mutation.agent_token: Destroying... [id=3967274967]
2022/02/08 13:14:23 [DEBUG] POST https://gitlab.com/api/v4/projects/32591477/terraform/state/groot
╷
│ Error: graphql server error: The resource that you are attempting to access does not exist or you don't have permission to perform this action
│ 
│ graphql server error: The resource that you are attempting to access does not exist or you don't have permission to perform this action
╵
```


This happens because the state gets out of sync.

So remove the state:
```
$ terraform state rm module.gitlab_kubernetes_agent.graphql_mutation.agent_token
2022/02/08 13:23:59 [DEBUG] POST https://gitlab.com/api/v4/projects/32591477/terraform/state/groot/lock
Acquiring state lock. This may take a few moments...
2022/02/08 13:24:00 [DEBUG] GET https://gitlab.com/api/v4/projects/32591477/terraform/state/groot
Removed module.gitlab_kubernetes_agent.graphql_mutation.agent_token
2022/02/08 13:24:00 [DEBUG] POST https://gitlab.com/api/v4/projects/32591477/terraform/state/groot?ID=98f72c8f-a8f0-f6cd-c001-c88b37eff3be
Successfully removed 1 resource instance(s).
2022/02/08 13:24:01 [DEBUG] DELETE https://gitlab.com/api/v4/projects/32591477/terraform/state/groot/lock   
```


```
$ ./install.sh destroy  
```


The same thing may happen when the agent is out of sync while trying to update.
```
$ ./install.sh create                                          
...
2022/02/08 15:02:10 [DEBUG] POST https://gitlab.com/api/v4/projects/32591477/terraform/state/groot?ID=91b90f55-ed9d-f7aa-9139-9289f5cef84a
╷
│ Error: Provider produced inconsistent final plan
│ 
│ When expanding the plan for module.gitlab_kubernetes_agent.graphql_mutation.agent_token to include new values learned so far during apply, provider
│ "registry.terraform.io/sullivtr/graphql" produced an invalid new value for .mutation_variables["agent_id"]: was cty.StringVal("gid://gitlab/Clusters::Agent/6584"), but now
│ cty.StringVal("gid://gitlab/Clusters::Agent/6586").
│ 
│ This is a bug in the provider, which should be reported in the provider's own issue tracker.
╵
```

- So goto your project in GitLab & remove the agent via the UI.
- Next remove the terraform state of these. 
```
terraform state rm module.gitlab_kubernetes_agent.graphql_mutation.agent_token; terraform state rm module.gitlab_kubernetes_agent.graphql_mutation.cluster_agent
```
Finally, rerun your TF.



## CURRENT LIMITATIONS
The current GitLab Agent for Kubernetes will only be accessable to groups / projects parallel to or under this project. If you register the agent with a project in a subgroup, the agent will not be accessable from outside said group. For this reason, I'm creating / managing the cluster from a project in my root level.

## Open Issues
- [NameSpace must be set as var](https://gitlab.com/gitlab-org/gitlab/-/issues/339465)
- [Context must be set as a var](https://gitlab.com/gitlab-org/gitlab/-/issues/346636)
- [There cannot be any Kubernetes clusters integrated to GitLab Org via certificate](https://gitlab.com/gitlab-org/gitlab/-/issues/335089)
- [Cluster Management Project Must be at Parent Level](https://gitlab.com/gitlab-org/gitlab/-/issues/34650)

## Related Projects
- [Kas EtoE Demo](https://gitlab.com/mark.cesario/kas/kas-etoe-demo)  

## TODO
1. Minimize [oauth_scopes](https://gitlab.com/sandlin/gke-deploy-env/-/blob/groot/gke_cluster.tf#L45-51) in node pool 

