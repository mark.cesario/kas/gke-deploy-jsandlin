/*
* Create Network for our TF cluster.
*/

// Create VPC
resource "google_compute_network" "vpc" {
 name                    = "${var.cluster_name}-vpc"
 auto_create_subnetworks = "false"
}

// Create Subnet
resource "google_compute_subnetwork" "subnet" {
 name          = "${var.cluster_name}-subnet"
 ip_cidr_range = var.subnet_cidr
 network       = "${var.cluster_name}-vpc"
 depends_on    = [google_compute_network.vpc]
 region      = var.region
}

// VPC firewall configuration
resource "google_compute_firewall" "firewall" {
  name    = "${var.cluster_name}-firewall"
  network = "${google_compute_network.vpc.name}"

  allow {
    protocol = "icmp"
  }

  allow {
    protocol = "tcp"
    ports    = ["22", "443"]
  }

  source_ranges = ["0.0.0.0/0"]
  depends_on    = [google_compute_network.vpc]
}
